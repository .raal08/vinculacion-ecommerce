

 function format(d){
      return `<table>
        
        <tr>
            <td>Correo:</td>
            <td>${d.Correo}</td>
        </tr>
        
        <tr>
            <td>Fecha:</td>
            <td>${d.Fecha}</td>
        </tr>
        <tr>
            <td>Hora:</td>
            <td>${d.Hora}</td>
        </tr>      
       
        <tr>
            <td>Total:</td>
            <td>$ ${d.Total}</td>
        </tr>
        <tr>
            <td>Pedido:</td>
            <td>
            ${d.Pedido.map(function(Pedido){                
                return`<ul>
                   
                    <li>Pedido:${Pedido.name}</li>
                    <li>Precio:$${Pedido.price}</li>
                    <li>Cantidad:${Pedido.quantity}</li>
                    <li>Total:$${Pedido.total}</li>
                </ul>
                `
            })}</td>
        </tr>
      </table>
      `;
  }

 $(document).ready(function(){setTimeout(function(){
    var table=$("#tableAdmin").DataTable({
        "data":final.data,
        select:"single",
        "columns":[
            {
                "className":'details-control',
                "ordenable":false,
                "data":null,
                "defaultContent":'',
                "render":function(){
                    return`<i style="hover:pointer" class="fa fa-plus-square"
                    aria-hidden="true"></i>`;
                },
                width:"15px"
            },
           
            {"data":"Orden"},
            {"data":"Fecha"},
            {"data":"Nombre"},
            {"data":"Total"}
            
        ],
        "Orden":[[1,'desc']]

     });
     

     $('#tableAdmin tbody').on('click','td.details-control',
     function(){
         var tr=$(this).closest('tr');
         var tdi = tr.find("i.fa");
         var row=table.row(tr);

         if(row.child.isShown()){
             row.child.hide();
              tr.removeClass('shown');
              tdi.first().removeClass('fa-minus-square');
              tdi.first().addClass('fa-plus-square');
         }
         else{
             row.child(format(row.data())).show();
             tr.addClass('shown');
             tdi.first().removeClass('fa-plus-square');
             tdi.first().addClass('fa-minus-square');

         }

     });
     
 },4000)
 });

 var firebaseConfig = {
    apiKey: "AIzaSyC3kpVq2yvOgQjLijtzAnDY-o08OM4_C4A",
    authDomain: "onlinestore-32a86.firebaseapp.com",
    databaseURL: "https://onlinestore-32a86-default-rtdb.firebaseio.com",
    projectId: "onlinestore-32a86",
    storageBucket: "onlinestore-32a86.appspot.com",
    messagingSenderId: "247135709517",
    appId: "1:247135709517:web:701a07624828a973876f04",
    measurementId: "G-HGFM81CB0R"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  var orden = firebase.database().ref('orden');

  orden.on('child_added',function(data){
      var ordenValue= data.val();
      fsales(ordenValue);

  });
  function fsales(params){
      final.data.push(params);

  }
  var final={
      "data":[]
  }