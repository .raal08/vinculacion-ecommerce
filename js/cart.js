var firebaseConfig = {
    apiKey: "AIzaSyC3kpVq2yvOgQjLijtzAnDY-o08OM4_C4A",
    authDomain: "onlinestore-32a86.firebaseapp.com",
    databaseURL: "https://onlinestore-32a86-default-rtdb.firebaseio.com",
    projectId: "onlinestore-32a86",
    storageBucket: "onlinestore-32a86.appspot.com",
    messagingSenderId: "247135709517",
    appId: "1:247135709517:web:701a07624828a973876f04",
    measurementId: "G-HGFM81CB0R"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

  
  function total(){
      var products = JSON.parse(localStorage.getItem('cart'));
      var total=0;
     
      for(let index = 0; index < products.length; index++){
          if(products[index].cart){
              total+=parseFloat(products[index].total);
          }
      }
      console.log(total);
      return total
  }

var con=0;
var con2=JSON.parse(localStorage.getItem('positions'));
function clean(){
    document.getElementById('table').innerHTML='';
    document.getElementById('total').innerHTML='';
    var cart_n = document.getElementById('cart_n');
    cart_n.innerHTML='';
    localStorage.clear();
}

function remove(id){
    var products=JSON.parse(localStorage.getItem('cart'));
        for(let index =0; index < products.length; index++){
            if(products[index].id==id){
                var x = products[index].id;
                products.splice(index,1);
                localStorage.setItem('cart',JSON.stringify(products));
                total();
                for(let index2=0; index2 < con2.length; index2++){
                    if(x==con2[index2]){
                        con2.splice(index2,1);
                        localStorage.setItem('positions',JSON.stringify(con2));
                    }else{

                    }
                }updateCart();
            }else{
                updateCart();
            }
        }

}


function updateCart(){
    con=0;
    var cart_n = document.getElementById('cart_n');
    var products = JSON.parse(localStorage.getItem('cart'));
    cart_n.innerHTML=`[${products.length}]`;
    document.getElementById('table').innerHTML='';
    for(let index=0; index < con2.length; index++ ){
        var position=con2[index];
        for(let index3 =0; index3 < products.length; index3++){
            if(position == products[index3].id){
                
                document.getElementById('table').innerHTML+=`
                <tr>
                <th scope="row">${con+1}</th>
                <td><button class="waves-effect waves-light btn red darken-4"
                onclick="remove(${products[index3].id})">X</button></td>
                <td><img style="width:90px;" src="${products[index3].url}"> </td>
                <td>${products[index3].name}</td>   
                <td>
                <button class="waves-effect waves-light btn purple darken-4"
                onclick="reduceAmount(${products[index3].id})">-</button>
                <input style="width:6rem;" id="${products[index3].quantity}" 
                value="${products[index3].quantity}" disabled>
                <button class="waves-effect waves-light btn purple darken-4"
                onclick="addAmount(${products[index3].id})">+</button>
                </td>  
                <td>$${products[index3].price*products[index3].quantity}</td>  
                </tr> 
                `
                products[index3].total=products[index3].price*products[index3].quantity
                localStorage.setItem('cart',JSON.stringify(products));
            }else{

            }

        }con=con+1;
    }
    if(total() === 0){
        document.getElementById('total').innerHTML='';
    }else{
        document.getElementById('total').innerHTML=`<tr>
        <th></th>
        <td></td>
        <td></td>
        <td></td>

        <td>
            <h5>Total:</h5>
        </td>
        <td>$${total()}</td>
        </tr>
        <tr>
        <th ></th>
        <td></td>
        <td></td>
       
        <td></td>
        
        <td>
            <button id="btnClean" onclick="clean()" class="btn text-white
            btn-warning"> Vaciar </button>
        </td>
        <td >
            <button id="modal" href="#modal2" data-toggle="modal" class="btn 
            btn-success"> Realizar pedido </button>
        </td>
        </tr>
        `;
    }

}
function reduceAmount(id){
    var products =JSON.parse(localStorage.getItem('cart'));
    for ( let index=0; index<products.length; index++){
        if (products[index].id==id){
        if(products[index].quantity > 1){
            products[index].quantity=parseInt(products[index]
                .quantity)-1;
                localStorage.setItem("cart",JSON.stringify(products));
                
            }updateCart();
        }else{
            updateCart();
        }
    }
}

function addAmount(id){
    var products =JSON.parse(localStorage.getItem('cart'));
    for ( let index=0; index<products.length; index++){
        if (products[index].id==id){
        if(products[index].quantity>0){
            products[index].quantity=parseInt(products[index]
                .quantity)+1;
                localStorage.setItem("cart",JSON.stringify(products));
                
        }updateCart();
    }else{
        updateCart();
    }
    }
}



function render(){


      var products = JSON.parse(localStorage.getItem('cart'));
      var cart_n=document.getElementById('cart_n');
      for(let index=0; index<products.length;index++){
        document.getElementById('table').innerHTML+=`
            <tr>
                <th scope="row">${con+1}</th>
                <td><button class="waves-effect waves-light btn red darken-4"
                onclick="remove(${products[index].id})">X</button></td>
                <td><img style="width:90px;" src="${products[index].url}"> </td>
                <td>${products[index].name}</td>   
                <td>
                <button class="waves-effect waves-light btn purple darken-4"
                onclick="reduceAmount(${products[index].id})">-</button>
                <input style="width:6rem;" id="${products[index].quantity}" 
                value="${products[index].quantity}" disabled>
                <button class="waves-effect waves-light btn purple darken-4"
                onclick="addAmount(${products[index].id})">+</button>
                </td>  
                <td>$${products[index].price*products[index].quantity}</td>  
            </tr> 
        `;
        products[index].total=products[index].price*products[index].quantity
        localStorage.setItem('cart',JSON.stringify(products));

    }
    if(total() === 0){
        
        document.getElementById('total').innerHTML='';
    }else{
        
        document.getElementById('total').innerHTML=`<tr>
        <th></th>
        <td></td>
        <td></td>
        <td></td>
        
        <td>
            <h5>Total:</h5>
        </td>
        <td>$${total()}</td>
        </tr>
        <tr>
        <th ></th>
        <td></td>
        <td></td>
        <td></td>
    
        
        <td>
            <button id="btnClean" onclick="clean()" class="btn text-white
            btn-warning"> Vaciar </button>
        </td>
        <td >
            <button id="modal" href="#modal2" data-toggle="modal" class="btn 
            btn-success"> Realizar pedido </button>
        </td>
        </tr>
        `;
    }
    cart_n.innerHTML=`[${products.length}]`;
    


}

$(document).ready(()=>{
    $('.modal2').modal();
    var Nombre=document.getElementById("Nombre");
    var Correo=document.getElementById("Correo");
    var d = new Date();
    var t = d.getTime();
    var orden=t-300;


 $("#formCart").submit(function(e){
        e.preventDefault();
        var products=JSON.parse(localStorage.getItem("cart"));
        var ordens = firebase.database().ref('orden');
        var newOrden = ordens.push();
        newOrden.set({
        /*firebase.firestore().collection("orden").add({*/
            
            Orden:orden,            
            Nombre:Nombre.value,
            Correo:Correo.value,
            Fecha:d.getDate()+"/"+(d.getMonth()+1)+"/"+d.getFullYear(),
            Hora:d.getHours()+":"+d.getMinutes()+":"+d.getSeconds(),
            Pedido:products,
            Total:total()
        })
                
        .then(()=>{
            swal({
                position:'center',
                type:'success',
                title:'compra realizada con éxito!',
                text:`La orden de compra es: ${orden}`,
                showConfirmButton:true,
                timer:500000
            });
            $('#modal2').modal('hide'), close;
            
            clean();
        })
    })
})