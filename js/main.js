var firebaseConfig = {
    apiKey: "AIzaSyC3kpVq2yvOgQjLijtzAnDY-o08OM4_C4A",
    authDomain: "onlinestore-32a86.firebaseapp.com",
    databaseURL: "https://onlinestore-32a86-default-rtdb.firebaseio.com",
    projectId: "onlinestore-32a86",
    storageBucket: "onlinestore-32a86.appspot.com",
    messagingSenderId: "247135709517",
    appId: "1:247135709517:web:701a07624828a973876f04",
    measurementId: "G-HGFM81CB0R"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  
 //GLOBAL

var products = [];
var cartItems=[];
var cart_n=document.getElementById('cart_n');

if(localStorage.getItem('positions')){
    var positions=[JSON.parse(localStorage.getItem('positions'))];
}else{
    var positions=[];
}

//divs
 var pescadoDIV = document.getElementById("pescadoDIV");
 var camaronesDIV = document.getElementById("camaronesDIV");
 var chicharronesDIV = document.getElementById("chicharronesDIV");
 var pescadoEDIV = document.getElementById("pescadoEDIV"); 
 var arrozDIV = document.getElementById("arrozDIV");
 var cevichesDIV = document.getElementById("cevichesDIV");
 var especialesDIV = document.getElementById("especialesDIV");
 var bebidasDIV = document.getElementById("bebidasDIV");
 var cervezaDIV = document.getElementById("cervezaDIV");
 //INFORMATION
 var PESCADO = [ {id:1,cart:false,name:'Pescado Apanado',quantity:1,total:0, price:5},
    {id:2,cart:false,name:'Pescado Frito',quantity:1,total:0, price:5},
    {id:3,cart:false,name:'Pescado a la Plancha',quantity:1, total:0 ,price:5},
    {id:4,cart:false,name:'Pescado al Ajillo', quantity:1, total:0 ,price:6}];
 var CAMARONES = [
    {id:5,cart:false,name:'Camarones Apanado', quantity:1,total:0,price:6.50},
    {id:6,cart:false, name:'Camarones Reventado', quantity:1,total:0, price:6.50},
    {id:7,cart:false,name:'Camarones a la Plancha',quantity:1,total:0, price:7.50},
    {id:8,cart:false,name:'Camarones al Ajillo',quantity:1,total:0, price:6.50}
    ];
 var CHICHARRON = [
    {id:9,cart:false,name:'Chicharron de Pescado',quantity:1,total:0, price:6},
    {id:10,cart:false,name:'Chicharron de Camaron',quantity:1,total:0, price:7},
    {id:11,cart:false,name:'Chicharron de Pulpo',quantity:1, total:0,price:7},
    {id:12,cart:false,name:'Chicharron Mixto',quantity:1, total:0,price:7}
    ];
 var PESCADOE = [
    {id:13,cart:false,name:'Camotillo Frito',quantity:1,total:0, price:12.50},
    {id:14,cart:false,name:'Camotillo al Ajillo',quantity:1, total:0,price:12.50},
    {id:15,cart:false,name:'Camotillo al Vapor',quantity:1,total:0, price:9.50},
    {id:16,cart:false,name:'Estofado de Camotillo',quantity:1,total:0, price:9.50},
    {id:17,cart:false,name:'Estofado de Mariscos',quantity:1, total:0,price:8.50}
    ];
 var ARROZ = [
    {id:18,cart:false,name:'Arroz con Camaron',quantity:1, total:0,price:7},
    {id:19,cart:false,name:'Arroz con Concha',quantity:1, total:0,price:7},
    {id:20,cart:false,name:'Arroz con Pulpo',quantity:1, total:0,price:7},
    {id:21,cart:false,name:'Arroz Mixto',quantity:1,total:0, price:8},
    {id:22,cart:false,name:'Arroz Marinero',quantity:1,total:0, price:9}
    ];
 var CEVICHE = [
    {id:23,cart:false,name:'Ceviche de Pescado',quantity:1,total:0, price:5},
    {id:24,cart:false,name:'Ceviche de Camaron',quantity:1,total:0, price:7},
    {id:25,cart:false,name:'Ceviche de Concha',quantity:1, total:0,price:7},
    {id:26,cart:false,name:'Ceviche de Pulpo',quantity:1,total:0, price:8},
    {id:27,cart:false,name:'Ceviche Mixto',quantity:1,total:0, price:8},
    {id:28,cart:false,name:'Ceviche Marinero',quantity:1, total:0,price:9}
    ];
 var ESPECIAL = [
    {id:29,cart:false,name:'Conchas Asadas',quantity:1,total:0, price:7},
    {id:30,cart:false,name:'Ensalada de Mariscos',quantity:1,total:0, price:6},
    {id:31,cart:false,name:'Pulpo al Ajillo',quantity:1, total:0,price:7},
    {id:32,cart:false,name:'Bandera',quantity:1,total:0, price:10},
    {id:33,cart:false,name:'Bandera Especial',quantity:1,total:0, price:12},
    {id:34,cart:false,name:'Bandera Familiar',quantity:1,total:0, price:30}
    ];
 var BEBIDAS = [
    {id:35,cart:false,name:'Jarra de jugo Natural',quantity:1,total:0, price:3.50},
    {id:36,cart:false,name:'Cola 2L',quantity:1,total:0, price:2.75},
    {id:37,cart:false,name:'Cola 1.30L ',quantity:1,total:0, price:1.75},
    {id:38,cart:false,name:'Cola Mediana',quantity:1,total:0, price:0.75},
    {id:39,cart:false,name:'Gatorade',quantity:1,total:0, price:1.25}
    ];
 var CERVEZA = [
    {id:40,cart:false,name:'Pilsener',quantity:1,total:0, price:1.75},
    {id:41,cart:false,name:'Pilsener Light',quantity:1,total:0, price:1.75},
    {id:42,cart:false,name:'Club Grande',quantity:1, total:0,price:2.00},
    {id:43,cart:false,name:'Club Pequeña',quantity:1,total:0, price:1.50},
    {id:44,cart:false,name:'Corona',quantity:1, total:0,price:3.00}
    ];


    function HTMLpecadoProduct(con){
        let URL = `img/pescadoF/pesf${con}.jpeg`;
        let btn = `btnPescadoF${con}`;
        return `
         <div class="col-md-4">
            <div class="card mb-4 shadow-sm">
                     <img class= "card-img-top" style="height:16rem;" src="${URL}"
                       alt="Card image cap"></img>
                   <div class="card-body">
                        <i style="color:orange;" class="fa fa-star"></i>  
                        <i style="color:orange;" class="fa fa-star"></i>   
                        <i style="color:orange;" class="fa fa-star"></i>   
                        <i style="color:orange;" class="fa fa-star"></i>   
                        <i style="color:orange;" class="fa fa-star"></i>   
                        <p class="card-text">${PESCADO[con- 1].name}</p>                
                        <p class="card-text">Precio: $${PESCADO[con-1].price}</p>
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="btn-group">
                                <button type="button" onclick="cart2('${PESCADO[con-1].id}','${PESCADO[con-1].cart}','${PESCADO[con-1].name}','${PESCADO[con-1].quantity}','${PESCADO[con-1].total}',
                                '${PESCADO[con-1].price}','${URL}','${con}','${btn}')"
                                class="btn btn-sm btn-outline-secondary"><a style="color:inherit;"
                                href="cart.html">Comprar</a></button>
                                <button id='${btn}' type="button" onclick="cart('${PESCADO[con-1].id}','${PESCADO[con-1].cart}','${PESCADO[con-1].name}','${PESCADO[con-1].quantity}','${PESCADO[con-1].total}',
                                '${PESCADO[con-1].price}','${URL}','${con}','${btn}')"
                                class="btn btn-sm btn-outline-secondary">Agregar</button>
                            </div>                 
                        </div>
                    </div>               
                </div>
            </div>
        `
    }
    function HTMLcamaronProduct(con){
        let URL = `img/camarones/cam${con}.jpeg`;
        let btn = `btnCamaron${con}`;
        return `
         <div class="col-md-4">
            <div class="card mb-4 shadow-sm">
                     <img class= "card-img-top" style="height:16rem;" src="${URL}"
                       alt="Card image cap"></img>
                   <div class="card-body">
                        <i style="color:orange;" class="fa fa-star"></i>   
                        <i style="color:orange;" class="fa fa-star"></i>  
                        <i style="color:orange;" class="fa fa-star"></i>   
                        <i style="color:orange;" class="fa fa-star"></i>   
                        <i style="color:orange;" class="fa fa-star"></i>   
                        <p class="card-text">${CAMARONES[con-1].name}</p>                
                        <p class="card-text">Preccio: $${CAMARONES[con-1].price}</p>            
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="btn-group">
                                <button type="button" onclick="cart2('${CAMARONES[con-1].id}','${CAMARONES[con-1].cart}','${CAMARONES[con-1].name}','${CAMARONES[con-1].quantity}','${CAMARONES[con-1].total}',
                                '${CAMARONES[con-1].price}','${URL}','${con}','${btn}')"
                                class="btn btn-sm btn-outline-secondary"><a style="color:inherit;"
                                href="cart.html">Comprar</a></button>
                                <button id='${btn}' type="button" onclick="cart('${CAMARONES[con-1].id}',${CAMARONES[con-1].cart},'${CAMARONES[con-1].name}','${CAMARONES[con-1].quantity}','${CAMARONES[con-1].total}',
                                '${CAMARONES[con-1].price}','${URL}','${con}','${btn}')"
                                class="btn btn-sm btn-outline-secondary">Agregar</button>
                            </div>                    
                        </div>
                    </div>               
                </div>
            </div>
        `
    }
    function HTMLchicharronProduct(con){
        let URL = `img/chicharrones/chi${con}.jpeg`;
        let btn = `btnChicharron${con}`;
        return `
         <div class="col-md-4">
            <div class="card mb-4 shadow-sm">
                     <img class= "card-img-top" style="height:16rem;" src="${URL}"
                       alt="Card image cap"></img>
                   <div class="card-body">
                        <i style="color:orange;" class="fa fa-star"></i>   
                        <i style="color:orange;" class="fa fa-star"></i>   
                        <i style="color:orange;" class="fa fa-star"></i>   
                        <i style="color:orange;" class="fa fa-star"></i> 
                        <i style="color:orange;" class="fa fa-star"></i>    
                        <p class="card-text">${CHICHARRON[con-1].name}</p>                
                        <p class="card-text">Preccio: $${CHICHARRON[con-1].price}</p>            
                        <div class="d-flex justify-content-between align-items-center">
                            <div  class="btn-group">
                                <button type="button" onclick="cart2('${CHICHARRON[con-1].id}','${CHICHARRON[con-1].cart}',
                                    '${CHICHARRON[con-1].name}','${CHICHARRON[con-1].quantity}','${CHICHARRON[con-1].total}','${CHICHARRON[con-1].price}','${URL}','${con}','${btn}')"
                                class="btn btn-sm btn-outline-secondary"><a style="color:inherit;"
                                href="cart.html">Comprar </a></button>
                                <button id="${btn}" type="button" onclick="cart('${CHICHARRON[con-1].id}','${CHICHARRON[con-1].cart}','${CHICHARRON[con-1].name}','${CHICHARRON[con-1].quantity}','${CHICHARRON[con-1].total}',
                                '${CHICHARRON[con-1].price}','${URL}','${con}','${btn}')"
                                class="btn btn-sm btn-outline-secondary">Agregar</button>
                            </div>                                         
                        </div>
                    </div>               
            </div>
         </div>
        `
    }
    function HTMLpescadoEProduct(con){
        let URL = `img/pescadoE/pese${con}.jpeg`;
        let btn = `btnPescadoE${con}`;
        return `
         <div class="col-md-4">
            <div class="card mb-4 shadow-sm">
                     <img class= "card-img-top" style="height:16rem;" src="${URL}"
                       alt="Card image cap"></img>
                   <div class="card-body">
                        <i style="color:orange;" class="fa fa-star"></i>   
                        <i style="color:orange;" class="fa fa-star"></i>   
                        <i style="color:orange;" class="fa fa-star"></i>   
                        <i style="color:orange;" class="fa fa-star"></i> 
                        <i style="color:orange;" class="fa fa-star"></i>    
                        <p class="card-text">${PESCADOE[con-1].name}</p>                
                        <p class="card-text">Preccio: $${PESCADOE[con-1].price}</p>            
                        <div class="d-flex justify-content-between align-items-center">
                            <div  class="btn-group">
                                <button type="button" onclick="cart2('${PESCADOE[con-1].id}','${PESCADOE[con-1].cart}',
                                    '${PESCADOE[con-1].name}','${PESCADOE[con-1].quantity}','${PESCADOE[con-1].total}','${PESCADOE[con-1].price}','${URL}','${con}','${btn}')"
                                class="btn btn-sm btn-outline-secondary"><a style="color:inherit;"
                                href="cart.html">Comprar </a></button>
                                <button id="${btn}" type="button" onclick="cart('${PESCADOE[con-1].id}','${PESCADOE[con-1].cart}','${PESCADOE[con-1].name}','${PESCADOE[con-1].quantity}','${PESCADOE[con-1].total}',
                                '${PESCADOE[con-1].price}','${URL}','${con}','${btn}')"
                                class="btn btn-sm btn-outline-secondary">Agregar</button>
                            </div>                                         
                        </div>
                    </div>               
            </div>
         </div>
        `
    }
    function HTMLarrozProduct(con){
        let URL = `img/arroz/arroz${con}.jpeg`;
        let btn = `btnArroz${con}`;
        return `
         <div class="col-md-4">
            <div class="card mb-4 shadow-sm">
                     <img class= "card-img-top" style="height:16rem;" src="${URL}"
                       alt="Card image cap"></img>
                   <div class="card-body">
                        <i style="color:orange;" class="fa fa-star"></i>   
                        <i style="color:orange;" class="fa fa-star"></i>   
                        <i style="color:orange;" class="fa fa-star"></i>   
                        <i style="color:orange;" class="fa fa-star"></i> 
                        <i style="color:orange;" class="fa fa-star"></i>    
                        <p class="card-text">${ARROZ[con-1].name}</p>                
                        <p class="card-text">Preccio: $${ARROZ[con-1].price}</p>            
                        <div class="d-flex justify-content-between align-items-center">
                            <div  class="btn-group">
                                <button type="button" onclick="cart2('${ARROZ[con-1].id}','${ARROZ[con-1].cart}',
                                    '${ARROZ[con-1].name}','${ARROZ[con-1].quantity}','${ARROZ[con-1].total}','${ARROZ[con-1].price}','${URL}','${con}','${btn}')"
                                class="btn btn-sm btn-outline-secondary"><a style="color:inherit;"
                                href="cart.html">Comprar </a></button>
                                <button id="${btn}" type="button" onclick="cart('${ARROZ[con-1].id}','${ARROZ[con-1].cart}',
                                '${ARROZ[con-1].name}','${ARROZ[con-1].quantity}','${ARROZ[con-1].total}','${ARROZ[con-1].price}','${URL}','${con}','${btn}')"
                                class="btn btn-sm btn-outline-secondary">Agregar</button>
                            </div>                                         
                        </div>
                    </div>               
            </div>
         </div>
        `
    }
    function HTMLcevicheProduct(con){
        let URL = `img/ceviches/cevi${con}.jpeg`;
        let btn = `btnCeviche${con}`;
        return `
         <div class="col-md-4">
            <div class="card mb-4 shadow-sm">
                     <img class= "card-img-top" style="height:16rem;" src="${URL}"
                       alt="Card image cap"></img>
                   <div class="card-body">
                        <i style="color:orange;" class="fa fa-star"></i>   
                        <i style="color:orange;" class="fa fa-star"></i>   
                        <i style="color:orange;" class="fa fa-star"></i>   
                        <i style="color:orange;" class="fa fa-star"></i> 
                        <i style="color:orange;" class="fa fa-star"></i>    
                        <p class="card-text">${CEVICHE[con-1].name}</p>                
                        <p class="card-text">Preccio: $${CEVICHE[con-1].price}</p>            
                        <div class="d-flex justify-content-between align-items-center">
                            <div  class="btn-group">
                                <button type="button" onclick="cart2('${CEVICHE[con-1].id}','${CEVICHE[con-1].cart}',
                                    '${CEVICHE[con-1].name}','${CEVICHE[con-1].quantity}','${CEVICHE[con-1].total}','${CEVICHE[con-1].price}','${URL}','${con}','${btn}')"
                                class="btn btn-sm btn-outline-secondary"><a style="color:inherit;"
                                href="cart.html">Comprar </a></button>
                                <button id="${btn}" type="button" onclick="cart('${CEVICHE[con-1].id}','${CEVICHE[con-1].cart}',
                                '${CEVICHE[con-1].name}','${CEVICHE[con-1].quantity}','${CEVICHE[con-1].total}','${CEVICHE[con-1].price}','${URL}','${con}','${btn}')"
                                class="btn btn-sm btn-outline-secondary">Agregar</button>
                            </div>                                         
                        </div>
                    </div>               
            </div>
         </div>
        `
    }
    function HTMLespecialProduct(con){
        let URL = `img/especiales/especial${con}.jpeg`;
        let btn = `btnEspecial${con}`;
        return `
         <div class="col-md-4">
            <div class="card mb-4 shadow-sm">
                     <img class= "card-img-top" style="height:16rem;" src="${URL}"
                       alt="Card image cap"></img>
                   <div class="card-body">
                        <i style="color:orange;" class="fa fa-star"></i>   
                        <i style="color:orange;" class="fa fa-star"></i>   
                        <i style="color:orange;" class="fa fa-star"></i>   
                        <i style="color:orange;" class="fa fa-star"></i> 
                        <i style="color:orange;" class="fa fa-star"></i>    
                        <p class="card-text">${ESPECIAL[con-1].name}</p>                
                        <p class="card-text">Preccio: $${ESPECIAL[con-1].price}</p>            
                        <div class="d-flex justify-content-between align-items-center">
                            <div  class="btn-group">
                                <button type="button" onclick="cart2('${ESPECIAL[con-1].id}','${ESPECIAL[con-1].cart}',
                                    '${ESPECIAL[con-1].name}','${ESPECIAL[con-1].quantity}','${ESPECIAL[con-1].total}','${ESPECIAL[con-1].price}','${URL}','${con}','${btn}')"
                                class="btn btn-sm btn-outline-secondary"><a style="color:inherit;"
                                href="cart.html">Comprar </a></button>
                                <button id="${btn}" type="button" onclick="cart('${ESPECIAL[con-1].id}','${ESPECIAL[con-1].cart}',
                                '${ESPECIAL[con-1].name}','${ESPECIAL[con-1].quantity}','${ESPECIAL[con-1].total}',
                                '${ESPECIAL[con-1].price}','${URL}','${con}','${btn}')"
                                class="btn btn-sm btn-outline-secondary">Agregar</button>
                            </div>                                         
                        </div>
                    </div>               
            </div>
         </div>
        `
    }
    function HTMLbebidaProduct(con){
        let URL = `img/bebidas/bebi${con}.jpeg`;
        let btn = `btnBebida${con}`;
        return `
         <div class="col-md-4">
            <div class="card mb-4 shadow-sm">
                     <img class= "card-img-top" style="height:16rem;" src="${URL}"
                       alt="Card image cap"></img>
                   <div class="card-body">
                        <i style="color:orange;" class="fa fa-star"></i>   
                        <i style="color:orange;" class="fa fa-star"></i>   
                        <i style="color:orange;" class="fa fa-star"></i>   
                        <i style="color:orange;" class="fa fa-star"></i> 
                        <i style="color:orange;" class="fa fa-star"></i>    
                        <p class="card-text">${BEBIDAS[con-1].name}</p>                
                        <p class="card-text">Preccio: $${BEBIDAS[con-1].price}</p>            
                        <div class="d-flex justify-content-between align-items-center">
                            <div  class="btn-group">
                                <button type="button" onclick="cart2('${BEBIDAS[con-1].id}','${BEBIDAS[con-1].cart}',
                                    '${BEBIDAS[con-1].name}','${BEBIDAS[con-1].quantity}','${ESPECIAL[con-1].total}','${BEBIDAS[con-1].price}','${URL}','${con}','${btn}')"
                                class="btn btn-sm btn-outline-secondary"><a style="color:inherit;"
                                href="cart.html">Comprar </a></button>
                                <button id="${btn}" type="button" onclick="cart('${BEBIDAS[con-1].id}','${BEBIDAS[con-1].cart}','${BEBIDAS[con-1].name}','${BEBIDAS[con-1].quantity}','${ESPECIAL[con-1].total}',
                                '${BEBIDAS[con-1].price}','${URL}','${con}','${btn}')"
                                class="btn btn-sm btn-outline-secondary">Agregar</button>
                            </div>                                         
                        </div>
                    </div>               
            </div>
         </div>
        `
    }
    function HTMLcervezaProduct(con){
        let URL = `img/cerveza/cerv${con}.jpeg`;
        let btn = `btnCerveza${con}`;
        return `
         <div class="col-md-4">
            <div class="card mb-4 shadow-sm">
                     <img class= "card-img-top" style="height:16rem;" src="${URL}"
                       alt="Card image cap"></img>
                   <div class="card-body">
                        <i style="color:orange;" class="fa fa-star"></i>   
                        <i style="color:orange;" class="fa fa-star"></i>   
                        <i style="color:orange;" class="fa fa-star"></i>   
                        <i style="color:orange;" class="fa fa-star"></i> 
                        <i style="color:orange;" class="fa fa-star"></i>    
                        <p class="card-text">${CERVEZA[con-1].name}</p>                
                        <p class="card-text">Preccio: $${CERVEZA[con-1].price}</p>            
                        <div class="d-flex justify-content-between align-items-center">
                            <div  class="btn-group">
                                <button type="button" onclick="cart2('${CERVEZA[con-1].id}','${CERVEZA[con-1].cart}',
                                    '${CERVEZA[con-1].name}','${CERVEZA[con-1].quantity}','${ESPECIAL[con-1].total}','${CERVEZA[con-1].price}','${URL}','${con}','${btn}')"
                                class="btn btn-sm btn-outline-secondary"><a style="color:inherit;"
                                href="cart.html">Comprar </a></button>
                                <button id="${btn}" type="button" onclick="cart('${CERVEZA[con-1].id}','${CERVEZA[con-1].cart}','${CERVEZA[con-1].name}','${CERVEZA[con-1].quantity}','${ESPECIAL[con-1].total}',
                                '${CERVEZA[con-1].price}','${URL}','${con}','${btn}')"
                                class="btn btn-sm btn-outline-secondary">Agregar</button>
                            </div>                                         
                        </div>
                    </div>               
            </div>
         </div>
        `
    }

//----------------
function animation(){
    const toast= swal.mixin({
        toast:true,
        position:'top-end',
        showConfirmButton:false,
        timer:1000
    });

    toast({
        type:'success',
        title: 'Agregado al carrito de compras'
    });
}
//cart functions
function cart(id,cart,name,quantity,total,price,url,con,btncart){
    var item={
        id:id,
        cart:true,
        name:name,
        quantity:quantity,
        total:total,
        price:price,
        url:url
    }
    positions.push(id);
    localStorage.setItem("positions",JSON.stringify(positions));
    cartItems.push(item);
    let storage=JSON.parse(localStorage.getItem("cart"));
    if(storage==null){
        products.push(item);
        localStorage.setItem("cart",JSON.stringify(products));
    }else{
        products=JSON.parse(localStorage.getItem("cart"));
        products.push(item);
        localStorage.setItem("cart",JSON.stringify(products));        
    }
    products=JSON.parse(localStorage.getItem("cart"));
    cart_n.innerHTML=`[${products.length}]`;
    document.getElementById(btncart).style.display="none";
    animation();    
}
function cart2(id,cart,name,quantity,total,price,url,con,btncart){
    var item={
        id:id,
        cart:true,
        name:name,
        quantity:quantity,
        total:total,
        price:price,
        url:url
    }
    positions.push(id);
    localStorage.setItem("positions",JSON.stringify(positions));
    cartItems.push(item);
    let storage=JSON.parse(localStorage.getItem("cart"));
    if(storage==null){
        products.push(item);
        localStorage.setItem("cart",JSON.stringify(products));
    }else{
        products=JSON.parse(localStorage.getItem("cart"));
        products.push(item);
        localStorage.setItem("cart",JSON.stringify(products));        
    }
    products=JSON.parse(localStorage.getItem("cart"));
    cart_n.innerHTML=`[${products.length}]`;
    document.getElementById(btncart).style.display="none";
    animation();    
}


/// RENDER

function render(){
    for(let index = 1; index <= 4; index++){
        pescadoDIV.innerHTML+= `${HTMLpecadoProduct(index)}`;
        camaronesDIV.innerHTML+=`${HTMLcamaronProduct(index)}`;
        chicharronesDIV.innerHTML+=`${HTMLchicharronProduct(index)}`;
    }
    for(let index = 1; index <= 5 ; index++){              
        pescadoEDIV.innerHTML+=`${HTMLpescadoEProduct(index)}`;
        arrozDIV.innerHTML+=`${HTMLarrozProduct(index)}`;
        cevichesDIV.innerHTML+=`${HTMLcevicheProduct(index)}`
        bebidasDIV.innerHTML+=`${HTMLbebidaProduct(index)}`;
        cervezaDIV.innerHTML+=`${HTMLcervezaProduct(index)}`;
    }
    for(let index = 1; index <= 6 ; index++){        
        especialesDIV.innerHTML+=`${HTMLespecialProduct(index)}`;       
        
        
    }
    
    if(localStorage.getItem("cart")==null){
    }else{
        products=JSON.parse(localStorage.getItem("cart"));
        cart_n.innerHTML=`[${products.length}]`;
    }
}